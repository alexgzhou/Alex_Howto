GitLab是GitHub的替代产品: 开源, 功能基本相同, 可以从GitHub导入库, 免费建立私有库...
[网址:](https://gitlab.com/); 
[文档:](http://doc.gitlab.com/ce/gitlab-basics/README.html);

1. 基本操作(注意是在安装了Github for windows的环境下):

    (1) [设置用户名和邮箱:](http://doc.gitlab.com/ce/gitlab-basics/start-using-git.html)
        ```note
            查看git是否安装:
            `$ git --version`
            设置git用户名(gitlab的账号最好和github的账号一样):
            `$ git config --global user.name alexgzhou`
            查看用户名:
            `$ git config --global user.name`
            设置邮箱(gitlab的账号最好和github的账号一样)
            `$ git config --global user.email alexgzhou@163.com`
            查看邮箱:
            `$ git config --global user.email`
            查看所有全局设置:
            `$ git config --global --list`
            搞定!!!!!!
        ```

    (2) [设置SSH Key](http://doc.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html)
        ```note
            查看是否有SSH Key:
            `$ cat ~/.ssh/id_rsa.pub`  # Linux和Windows都适用
            生成一对SSH Key(一般路径是当前用户目录下的.ssh/文件夹, 默认文件名为id_rsa*; 如果安装了github for windows, 还可以看到github_rsa的一对密钥):
            `$ ssh-keygen -t rsa -C "alexgzhou@163.com"`  # Linux和Windows都适用(注意要将私钥id_rsa.rsa改名为id_rsa)
            拷贝公钥的内容(用下面命令行或直接用文本编辑器打开拷贝):
                Windows:
                `$ clip < ~/.ssh/id_rsa.pub`
                Mac:
                `$ pbcopy < ~/.ssh/id_rsa.pub`
                GNU/Linux (requires xclip):
                `$ xclip -sel clip < ~/.ssh/id_rsa.pub`
            将拷贝的密钥复制到https://gitlab.com/的Profile Settings的SSH Keys下面;
            搞定!!!!!!
        ```
    
    **这样就可以在命令行中用git指令操作gitlab上的remote库了**

2. GitHub for Windows连接GitLab(先做好上面的基本步骤)
    
    (1) 在GitHub for Windows中新建(Create)一个库(如nodeServerGitLab), Undo most recent commit取消其中自动新建的.gitignore等文件;

    (2) 在GitHub for Windows该库(nodeServerGitLab)的Repository Settings的Remote中将gitlab.com中的git库地址粘贴进去;

    (3) 在GitHub for Windows该库(nodeServerGitLab)中打开Git Shell, 运行`$ git fetch`下载库信息, 然后运行`$ git pull origin dev`下载某个具体的分支;

    (4) 如果要在GitHub for Windows的图形界面中push到gitlab(注意不是shell中), 还需要把GitHub for Windows的SSH Key也添加到https://gitlab.com/的Profile Settings的SSH Keys下面;

    (5) .git文件夹里面包含了该git库的所有信息, 可以复制到任何地方形成该库的完全拷贝;

3. 新建上传一个git仓库(在gitlab.com上新建一个仓库后会有instruction, 如下)

    3.1 Command line instructions

    (1) Git global setup
        ```
        $ git config --global user.name "Alex Zhou"
        $ git config --global user.email "alexgzhou@163.com"
        ```

    (2) Create a new repository
        ```
        $ git clone git@gitlab.com:alexgzhou/proj.git
        $ cd proj
        $ touch README.md
        $ git add README.md
        $ git commit -m "add README"
        $ git push -u origin master
        ```

    (3) Existing folder or Git repository
        ```
        $ cd existing_folder
        $ git init
        $ git remote add origin git@gitlab.com:alexgzhou/proj.git
        $ git add .
        $ git commit
        $ git push -u origin master
        ```

    3.2 总结(?)

    (1) `$ mkdir proj & cd proj & git init`

    (2) create/copy some files

    (3) `$ git add --all & git commit -m init`

    (4) 在Gitlab/Github上新建一个仓库proj

    (5) `$ git remote add origin git@gitlab.com:alexgzhou/proj.git`

    (6) `$ git push -u origin master`