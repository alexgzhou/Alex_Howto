- -------------------------------
###How to modify a specified commit?
```bash
$ git rebase -i HEAD~3	//: HEAD后面的数字代表最新的3个commit
			//1.在跳出来的文本编辑器中将需要修改的commit前面的pick改为edit，保存并关闭
			//2.修改相应的文件并保存
$ git commit -a --amend --no-edit
$ git rebase --continue
```

- -------------------------------
###How to delete commits?
```bash
$ git rebase -i HEAD~3	//: HEAD后面的数字代表最新的3个commit
			//在跳出来的文本编辑器中将需要删除的commits的行删掉，保存并关闭
$ git push origin [branch_name] --force	//强制推送到远程服务器，删除之前同步的commits
```

- -------------------------------
###How to merge a specific commit in git?
```bash
$ git cherry-pick [SHA]
```

- -------------------------------
###从其他Branch合并过来，将多个commits合并成一个commit, 并修改commit message?

查看commits列表
```bash
$ git log --pretty=oneline
```

合并commits成一个commit, 数字3表示需要合并的commits数量
```bash
$ git rebase --interactive HEAD~3
```

将跳出来的文本编辑器中的需要合并的commits前面的pick改成s, 代表squash, 如下:
pick b76d157 previous commit
s c59f218 commit need to be meld
s a931ac7 commit need to be meld
保存关闭文本编辑器

等待...跳出一个新的文本编辑器, 修改commit message(所有非#号开头的行都是commit message, 其中第一个非#号开头的行是message title, 后面非#号开头的行都是message body), 保存关闭.

等待...所有的commits合并成一个新的commit并带有新的commit message.

到此可以利用 `$ git cherry-pick [SHA]` 或者 Merge 将该commit合并到目标Branch中.

- -------------------------------
###How to modify a specific commit in git?
修改倒数第5个commit
```bash
$ git rebase -i HEAD~6
```

将跳出来的文本编辑器中的需要修改的commit前面的pick改成e, 保存关闭文本.

修改需要改变的文件并保存, 根据提示输入下面的指令:
```bash
$ git commit --amend

$ git add .
$ git commit --amend
$ git rebase --continue
```

期间跳出来的所有的文本编辑器, 如果不需要修改commit message, 也要保存该文本, 再退出编辑器.

- -------------------------------
###How to discard unstaged changes?
```bash
$ git checkout -- .
$ git clean -f
```

- -------------------------------
###How to fast-forward a branch to head?
```bash
$ git checkout master
$ git pull origin
```

- -------------------------------
Git force pull to overwrite local files
```bash
$ git fetch --all
$ git reset --hard origin/master
$ git pull origin master
```